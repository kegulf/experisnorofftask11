﻿using ExperisNoroffTask11.model;
using ExperisNoroffTask11.model.cash;
using System;
using System.Collections.Generic;

namespace ExperisNoroffTask11 {
    class Program {


        static void Main(string[] args) {

            Dictionary<string, double> merchandise = GetMerchandise();

            Console.WriteLine("HELLOOO there");
            Console.WriteLine("Welcome to the Odd market");

            Person shopper = RequestShopperInfo();
            MillionthCustomer(shopper);

            string userChoiceToRetry = "";
            do {
                Console.WriteLine("\n\nWhat can I do you for today? (type the fruit you want ex: apples)\n");
                string wantedItem = RequestWantedItem(merchandise);

                Console.WriteLine($"\nOk great, how many {wantedItem} do you want?");
                int numberOfItems = RequestAmountOfWantedItem();

                double price = numberOfItems * merchandise[wantedItem];
                Console.WriteLine($"\nOk, sweet! That'll be {price} kr\n");

                HandlePayment(shopper, price);

                Console.WriteLine("Do you want to shop more? (y/n)");
                userChoiceToRetry = Console.ReadLine();


            } while (userChoiceToRetry != "n");

            Console.WriteLine("Thank you for choosing an Odd market for your shopping need");
            Environment.Exit(0);
        }


        static Person RequestShopperInfo() {
            Console.WriteLine("What's your name?");

            Console.Write("First name: ");
            string firstName = Console.ReadLine();

            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            return new Person(firstName, lastName);
        }

        static void MillionthCustomer(Person luckyPerson) {
            Console.WriteLine($"\n\nCongratulations {luckyPerson.FirstName}, you are our 1.000.000 customer." +
                $"\nFor that you win the great sum of 1775 kr cash and 2000 on your account!!! My god! That's like 20 bucks!");

            SavingsCard shoppingCard = new SavingsCard("1231231231234");
            shoppingCard.Insert(2000);
            luckyPerson.PutNewCardInWallet(shoppingCard);

            //
            luckyPerson.CashInPayCheck(
                new Coin(5),
                new Note(1000),
                new Coin(20),
                new Note(200),
                new Note(500),
                new Note(50)
            );
        }


        static Dictionary<string, double> GetMerchandise() {

            Dictionary<string, double> merchandise = new Dictionary<string, double>();

            merchandise.Add("apples", 7.99);
            merchandise.Add("oranges", 8.45);
            merchandise.Add("lemons", 4.36);

            return merchandise;
        }

        static string RequestWantedItem(Dictionary<string, double> merchandise) {
            string userInput = "";
            do {

                foreach (string key in merchandise.Keys) 
                    Console.WriteLine($"- {key}, {merchandise[key]} a piece");
               
                Console.WriteLine();

                userInput = Console.ReadLine();

                if (!merchandise.ContainsKey(userInput.ToLower()))
                    Console.WriteLine("\nLet's try that again\n");

            } while (!merchandise.ContainsKey(userInput.ToLower()));

            return userInput;
        }

        static int RequestAmountOfWantedItem() {
            int numberOfItems = 0;

            do {
                

                try {
                    numberOfItems = int.Parse(Console.ReadLine());
                }

                catch (Exception e) {
                    if (e is FormatException) Console.WriteLine("\nPlease enter a valid positiv integer");
                }

            } while (numberOfItems < 1);

            return numberOfItems;
        }

        static string GetPaymentChoice() {
            string paymentChoice = "";

            do {
                Console.WriteLine("Do you wish to pay with cash or card?\n");
                paymentChoice = Console.ReadLine().ToLower();

                if (!paymentChoice.Equals("card") && !paymentChoice.Equals("cash"))
                    Console.WriteLine("Okay, let's try that again");

            } while (!paymentChoice.Equals("card") && !paymentChoice.Equals("cash"));

            return paymentChoice;
        }

        static void HandlePayment(Person shopper, double price) {
            double payment = 0;
            string paymentChoice = GetPaymentChoice();

            if (paymentChoice.Equals("card"))
                payment = shopper.PayWithSavingsCard(price);

            else if (paymentChoice.Equals("cash")) {

                foreach (Cash c in shopper.PayWithCash(price)) {
                    payment += c.Value;
                }
            }

            if (payment == 0) {
                Console.WriteLine("You seem to be broke");
            }

            else if (payment == price) {
                Console.WriteLine("Ok great, thank you, come again <3");
            }

            else if (payment > price) {

                int change = (int)Math.Round(payment - price);
                Dictionary<int, int> cashUnits = GetChange(change);
                List<Cash> changeInCash = new List<Cash>();

                Console.WriteLine($"You payed {payment} kr");
                Console.WriteLine($"That will give you {change} kr back");

                foreach (int key in cashUnits.Keys) {

                    if (key < 50) {
                        Coin coin = new Coin(key);
                        changeInCash.Add(coin);

                        Console.WriteLine($"- {cashUnits[key]} x {coin}");
                    }

                    else {
                        Note note = new Note(key);
                        changeInCash.Add(note);

                        Console.WriteLine($"- {cashUnits[key]} x {note}");
                    }
                }

                shopper.CashInPayCheck(changeInCash.ToArray());
            }
        }

        

        public static Dictionary<int, int> GetChange(int change) {

            // Norwegian Cash units
            int[] cashUnits = { 1000, 500, 200, 100, 50, 20, 10, 5, 1 };

            // First int is unit and second one is counter.
            Dictionary<int, int> unitCounter = new Dictionary<int, int>();

            for (int i = 0; i < cashUnits.Length; i++) {

                if (change != 0) {
                    int unit = cashUnits[i];

                    // As long as we can take a unit of the remaining change, do it
                    // And update the counter.
                    while (change >= unit) {
                        change -= unit;

                        if (unitCounter.ContainsKey(unit)) unitCounter[unit]++;
                        else unitCounter.Add(unit, 1);         
                    }
                }
            }
            return unitCounter;
        }
    }
}
