﻿using ExperisNoroffTask11.model.cash;
using System;
using System.Collections.Generic;

namespace ExperisNoroffTask11.model {
    class Wallet {

        #region Attributes

        private CreditCard creditCard;
        private SavingsCard savingsCard;
        private List<Cash> cash;
        private double cashBalance = 0;

        #endregion


        #region Constructors

        public Wallet() {
            cash = new List<Cash>();
        }


        public Wallet(SavingsCard savingsCard) {
            this.creditCard = null;
            this.savingsCard = savingsCard;

            cash = new List<Cash>();
        }

        public Wallet(CreditCard creditCard) {
            this.creditCard = creditCard;
            this.savingsCard = null;

            cash = new List<Cash>();
        }

        public Wallet(CreditCard creditCard, SavingsCard savingsCard) {
            this.creditCard = creditCard;
            this.savingsCard = savingsCard;

            cash = new List<Cash>();
        }
        #endregion


        #region Behaviours

        public List<Cash> RetrieveCash(double amount) {

            if (cashBalance < amount) {
                Console.WriteLine("Not enough cash in wallet");
                return null;
            }
            List<Cash> cashToReturn = new List<Cash>();

            double sum = 0;

            foreach (Cash c in cash) {
                cashToReturn.Add(c);
                sum += c.Value;

                cashBalance -= c.Value;

                if (sum >= amount) break;
            }

            foreach (Cash cashToRemove in cashToReturn) {
                cash.Remove(cashToRemove);
            }

            return cashToReturn;
        }
        

        public void AddCashToWallet(params Cash[] cashToAdd) {
            Console.WriteLine("Adding cash to wallet");

            cash.AddRange(cashToAdd);
            cash.Reverse(); // sort it so the biggest is on top.

            double sumOfAddedCash = 0;
            foreach (Cash c in cashToAdd) {
                cashBalance += c.Value;
                sumOfAddedCash += c.Value;
            }

            Console.WriteLine($"Cash added to wallet: {sumOfAddedCash}\n" +
                $"Total balance: {cashBalance}");
        }




        /// <summary>
        ///     Get financial balance, not counting credit.
        /// </summary>
        /// <returns></returns>
        public double GetTotalFinancialBalance() {
            return cashBalance + ((savingsCard != null) ? savingsCard.Balance : 0);
        }

        #endregion


        #region Overrides / Implementations

        public override string ToString() {
            return $"Wallet with {cashBalance} Kroner in Cash and " +
                $"{savingsCard.Balance} left on the savings card";
        }

        #endregion


        #region Accessor Methods

        public CreditCard CreditCard { get => creditCard; set => creditCard = value; }
        public SavingsCard SavingsCard { get => savingsCard; set => savingsCard = value; }
        public double CashBalance { get => cashBalance; }
        
        #endregion
    }
}
