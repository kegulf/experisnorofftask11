﻿using ExperisNoroffTask11.model.cash;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model {
    class Person : IComparable<Person> {

        #region Attributes

        private string firstName;
        private string lastName;
        private Wallet wallet;

        #endregion



        #region Constructors

        public Person(string firstName, string lastName) {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.wallet = new Wallet();
        }
        
        #endregion



        #region Behaviour

        public List<Cash> PayWithCash(double price) {
            return wallet.RetrieveCash(price);
        }



        public double PayWithCreditCard(double price) {

            CreditCard creditCard = wallet.CreditCard;

            if (creditCard != null)
                return creditCard.Withdraw(price);
            
            Console.WriteLine("No credit card found");
            return 0;
        }



        public double PayWithSavingsCard(double price) {

            SavingsCard savingsCard = wallet.SavingsCard;

            if (savingsCard != null)
                return savingsCard.Withdraw(price);

            Console.WriteLine("No savings card found");
            return 0;
        }



        public void CashInPayCheck(params Cash[] pay) {
            wallet.AddCashToWallet(pay);
        }


        public void PutNewCardInWallet(Card card) {
            if (card is CreditCard) wallet.CreditCard = (CreditCard) card;
            else if (card is SavingsCard) wallet.SavingsCard = (SavingsCard) card;
        }

        #endregion



        #region Overrides / Implementations

        public override string ToString() {
            return $"{firstName} {lastName}";       
        }



        public int CompareTo(Person other) {
            return other.wallet.GetTotalFinancialBalance().CompareTo(this.wallet.GetTotalFinancialBalance());
        }
   
        #endregion



        #region Accessor Methods

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }

        #endregion

    }
}
