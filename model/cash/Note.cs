﻿using System;

namespace ExperisNoroffTask11.model.cash {
    class Note : Cash {
        public Note(int value) : base(value) {

            if(value != 50 && value != 100 && value != 200 && value != 500 && value != 1000)
                Console.WriteLine("Valid note values are 50, 100, 200, 500, 1000");

        }

        public override string ToString() {
            return $"{Value} kroner note";
        }
    }
}
