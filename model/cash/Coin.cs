﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model.cash {
     class Coin : Cash {
        public Coin(int value) : base(value) {

            if (value != 1 && value != 5 && value != 10 && value != 20) {
                Console.WriteLine("Valid coin values are 1, 5, 10, 20");
            }
        }


        public override string ToString() {

            string krone = (Value == 1) ? "krone" : "kroner";

            return $"{Value} {krone} coin";
        }
    }
}
