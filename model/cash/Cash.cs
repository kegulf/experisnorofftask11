﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model.cash {
    abstract class Cash : IComparable<Cash> {
        private double value;

        public Cash(double value) {
            this.value = value;
        }

        public double Value { get => value;  }


        public override bool Equals(Object other) {
            if (other is Cash) return ( this.value == ((Cash)other).value );
            else return false;
        }

        public int CompareTo(Cash other) {
            return this.value.CompareTo(other.value);
        }
    }
}
