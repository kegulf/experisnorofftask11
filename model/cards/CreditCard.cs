﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model {
    class CreditCard : Card {

        private double creditLimit;


        public CreditCard(string accountNumber, double creditLimit) : base(accountNumber) {
            this.creditLimit = creditLimit;
        }



        public override double Withdraw(double amountToWithdraw) {

            if( this.Balance + creditLimit >= amountToWithdraw ) {
                Balance -= amountToWithdraw;
                return amountToWithdraw;
            }

            Console.WriteLine("Sorry Mac, your credit has ran out");
            return 0;  
        }



        public double CreditLimit { get => creditLimit; set => creditLimit = value; }

    }
}
