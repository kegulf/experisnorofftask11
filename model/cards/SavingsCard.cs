﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model {
    class SavingsCard : Card {
        public SavingsCard(string accountNumber ) : base(accountNumber ) { }

        public override double Withdraw(double amountToWithdraw) {
            if (this.Balance >= amountToWithdraw) {
                Balance -= amountToWithdraw;
                return amountToWithdraw;
            }

            Console.WriteLine("Insufficient funds, get a job!");
            return 0;
        }
    }
}
