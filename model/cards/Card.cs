﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask11.model {
    abstract class Card {

        private string accountNumber;
        private double balance;

        public Card(string accountNumber ) {
            this.accountNumber = accountNumber;
            this.balance = 0;
        }


        public void Insert(double amountToInsert) {
            if(amountToInsert > 0)
                balance += amountToInsert;
            else {
                Console.WriteLine("Minimum insert value is 1 NOK");
            }
        }

        public abstract double Withdraw(double amountToWithdraw);


        public string AccountNumber { get => accountNumber; set => accountNumber = value; }
        public double Balance { get => balance; set => balance = value; }
    }
}
